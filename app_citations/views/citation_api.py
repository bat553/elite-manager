from random import choice

from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

from app_citations.models import Citation
from app_citations.utils.errors_handler import ErrorHandler


@require_http_methods(["GET"])
def get_random(request):
    citations = Citation.objects.all()
    if len(citations):
        citation = choice(citations)
        if request.user.is_anonymous:
            return JsonResponse(citation.get_limited())
        return JsonResponse(citation.get_complete())

    return ErrorHandler(__name__, "not_found").json_response()
