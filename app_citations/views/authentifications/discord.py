from django.contrib.auth import authenticate, login
from django.shortcuts import redirect
from django.views.decorators.http import require_http_methods

from app_citations.models import Log
from app_citations.utils.authentification import oauth
from app_citations.utils.errors_handler import ErrorHandler


@require_http_methods(["GET"])
def login_discord(request):
    redirect_uri = request.build_absolute_uri("/oidc/discord/callback")
    return oauth.discord.authorize_redirect(request, redirect_uri)


@require_http_methods(["GET"])
def callback_discord(request):
    token = oauth.discord.authorize_access_token(request)
    resp = oauth.discord.get("users/@me", token=token)
    resp.raise_for_status()
    profile = resp.json()

    user = authenticate(request, email=profile.get("email"), discord_id=profile.get("id"))

    if user is not None:
        login(request, user)
        Log.objects.create(user=user, action="Connexion")
        return redirect("/api/citation")
    else:
        return ErrorHandler(__name__, "auth_failed").json_response()
