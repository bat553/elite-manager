from random import choice

from django.shortcuts import render
from django.views import defaults

from app_citations.models import Citation


def ihm_index(request):
    return render(request, "index.html", {"title": "Accueil"})


def citation_random(request):
    citations = Citation.objects.all()
    if len(citations):
        citation = choice(citations)
        if request.user.is_anonymous:
            return render(request, "citation.html", {"citation": citation.citation, "auteur": citation.auteur, "date": citation.auteur})
        return render(request, "citation.html", {"citation": citation.citation, "auteur": citation.auteur, "date": citation.auteur})

    return defaults.page_not_found()
