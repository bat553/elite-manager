"""
Création des données initiales pour l'application
"""
import logging

from django.contrib.auth.models import Group, Permission
from django.core.management import BaseCommand

GROUPS = {
    "Administrateurs": {
        "log entry": ["view"],
        "user": ["add", "delete", "change", "view"],
        "citation": ["add", "delete", "change", "view"],
        "log": ["view"]
    }
}


class Command(BaseCommand):
    help = "Génération des donnés de base pour l'application"

    def handle(self, *args, **options):
        for group_name in GROUPS.keys():
            new_group, created = Group.objects.get_or_create(name=group_name)

            for app_label in GROUPS[group_name].keys():
                print(app_label, group_name)
                for permission_name in GROUPS[group_name][app_label]:
                    name = "Can {} {}".format(permission_name, app_label)
                    print("Creating {}".format(name))

                    try:
                        model_add_perm = Permission.objects.get(name=name)
                    except Permission.DoesNotExist:
                        logging.warning("Permission not found with name '{}'.".format(name))
                        continue

                    new_group.permissions.add(model_add_perm)
