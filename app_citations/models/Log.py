from django.db import models
from django.utils.timezone import now


class Log(models.Model):

    timestamp = models.DateTimeField(default=now)
    action = models.CharField(max_length=20)

    user = models.ForeignKey("app_citations.Utilisateur", on_delete=models.CASCADE)
    citation = models.ForeignKey("app_citations.Citation", null=True, on_delete=models.CASCADE)

    data = models.TextField(null=True)

    def __str__(self):
        return "%s - %s - %s" % (self.user, self.action, self.citation)
