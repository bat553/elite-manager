import uuid

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _


class Utilisateur(AbstractUser):
    username = None

    uuid = models.UUIDField(max_length=36, primary_key=True, default=uuid.uuid4, editable=False)

    discord_id = models.CharField(max_length=255, unique=True, null=True, blank=True, editable=False)
    swano_id = models.CharField(max_length=255, unique=True, null=True, blank=True, editable=False)

    avatar = models.BinaryField(blank=True, null=True)
    email = models.EmailField(_("email address"), blank=True, unique=True)

    REQUIRED_FIELDS = []
    USERNAME_FIELD = "email"

    def save(self, *args, **kwargs):
        if (self.discord_id and self.swano_id):
            raise ValueError("Un seul service tier de connexion autorisé")
        if not self.discord_id and not self.swano_id:
            raise ValueError("Inscription sans OIDC impossible")

        self.set_unusable_password()
        super(Utilisateur, self).save(*args, **kwargs)

    def __str__(self):
        return self.email
