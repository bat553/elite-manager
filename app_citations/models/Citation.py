import uuid
from django.db import models

from app_citations.utils.errors_handler import ErrorHandler


class Citation(models.Model):
    uuid = models.UUIDField(max_length=36, primary_key=True, default=uuid.uuid4, editable=False)

    citation = models.TextField(max_length=650)
    auteur = models.CharField(max_length=50)
    date = models.CharField(max_length=50)

    timestamp = models.DateTimeField(auto_created=True)
    edited = models.DateTimeField(null=True)

    class Statuts(models.IntegerChoices):
        PUBLIC = 1
        PRIVATE = 2
        UNLISTED = 3
        INTERNAL = 4

    statut = models.IntegerField(choices=Statuts.choices, default=Statuts.PUBLIC)

    created_by = models.ForeignKey("app_citations.Utilisateur", on_delete=models.RESTRICT)

    def __str__(self):
        return "%s - %s - %s" % (self.citation, self.auteur, self.date)

    def get_complete(self) -> dict:
        return {
            "citation": self.citation,
            "auteur": self.auteur,
            "date": self.date,
            "timestamp": self.timestamp,
            "edited": self.edited,
            "created_by": self.created_by.email,
            "statut": self.statut
        }

    def get_limited(self) -> dict:
        return {
            "citation": self.citation,
            "auteur": self.auteur,
            "date": self.date,
            "timestamp": self.timestamp,
        }

    def clean(self):
        """
        Statut :
            1 = public
            2 = private
            3 = Non répertorié
            4 = Interne
        (CF #1)
        :return:
        """

        if self.statut <= 0 or self.statut > 4:
            raise ErrorHandler(__name__, "statut_invalid").error()
