from django.http import JsonResponse

from .errors_database import ERRORS_DATABASE

"""
Retourne une erreur correctement formatée suivant un dictionnaire unique
"""


class ErrorHandler:
    def __init__(self, module: str, code: str) -> None:
        self.module: str = module
        self.code: str = code

        self._error: dict = {}

        self._parse_error()

    def _parse_error(self) -> None:
        """
        Récupération de l'erreur dans un dictionnaire
        :return:
        """

        tab = self.module.split(".")

        if len(tab) < 3:
            raise ValueError("Nom du module mal formaté %s" % self.module)

        error = ERRORS_DATABASE
        for key in tab[1:]:
            error = error.get(key, None)
            if not error:
                raise ValueError("Erreur introuvable %s" % self.module)

        self._error = error.get(self.code, None)
        if "description" not in self._error or "severity" not in self._error or "http_code" not in self._error:
            raise ValueError("Erreur introuvable %s" % self.code)
        if not isinstance(self._error["severity"], int) or self._error["severity"] > 7 or self._error["severity"] < 0:
            raise ValueError("Sévérité introuvable : %s" % self._error["severity"])

    def json_response(self) -> JsonResponse:
        if self._error["severity"] < 4:
            # Faire une entrée dans les logs de l'application
            pass

        return JsonResponse(data=self._error, status=self._error.get("http_code", 200))

    def error(self):
        raise ValueError(self._error)
