"""
Dictionnaire des messages de retours

Format :
    {module} : valeur de __name__ sans app_citations
        {code} : code de l'erreur
            {severity} : SYSLOG code (0=EMERGENCY,1=ALERT,2=CRITICAL,3=ERROR,4=WARNING,5=NOTICE,6=INFO,7=DEBUG)
                            Si < 4 : Noter dans les logs de l'application
            {description} : Message pour l'utilisateur
            {http_code} : Code de retour

"""

ERRORS_DATABASE = {
    "views": {
        "citation_api": {"not_found": {"severity": 6, "description": "Aucune citation trouvée", "http_code": 404}},
        "authentifications": {
            "discord": {
                "auth_failed": {
                    "severity": 4,
                    "description": "L'authentification via discord a échoué",
                    "http_code": 403,
                }
            }
        },
    }
}
