import logging

from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import Group

from app_citations.models import Utilisateur


class DiscordAuthentificationBackend(ModelBackend):
    def authenticate(self, request, email=None, password=None, **kwargs):
        if "discord_id" not in kwargs:
            return
        try:
            user = Utilisateur.objects.get(discord_id=kwargs.get("discord_id"))
            user.email = email
            user.save()
        except Utilisateur.DoesNotExist:
            user = Utilisateur(discord_id=kwargs.get("discord_id"), email=email)
            user.save()
        return user

    def get_user(self, user_id):
        try:
            return Utilisateur.objects.get(pk=user_id)
        except Utilisateur.DoesNotExist:
            return None


class SwanoAuthentificationBackend(ModelBackend):
    def authenticate(self, request, email=None, password=None, **kwargs):
        if "swano_id" not in kwargs:
            return
        try:
            user = Utilisateur.objects.get(swano_id=kwargs.get("sub"))
            user.email = email
            user.first_name = kwargs.get('given_name', None)
            user.last_name = kwargs.get('family_name', None)
            user.is_staff = True
        except Utilisateur.DoesNotExist:
            user = Utilisateur(swano_id=kwargs.get("sub"), email=email, is_staff=True,
                               first_name=kwargs.get('given_name', None),
                               last_name=kwargs.get('family_name', None))
        try:
            admin_grp = Group.objects.get(name="Administrateurs")
            admin_grp.user_set.add(user)
        except Group.DoesNotExist:
            logging.warning("Groupe 'Administrateurs' introuvable")
        user.save()

        return user

    def get_user(self, user_id):
        try:
            return Utilisateur.objects.get(pk=user_id)
        except Utilisateur.DoesNotExist:
            return None
