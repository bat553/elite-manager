from authlib.integrations.django_client import OAuth
from django.conf import settings

oauth = OAuth()
oauth.register(name="discord", **settings.OAUTH_CLIENTS["discord"])
oauth.register(name="swano", **settings.OAUTH_CLIENTS["swano"])
