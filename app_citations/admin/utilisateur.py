from django.contrib import admin

from app_citations.admin import custom_admin
from app_citations.models import Utilisateur


@admin.register(Utilisateur, site=custom_admin)
class UtilisateurAdmin(admin.ModelAdmin):
    fields = (
        "uuid",
        "first_name",
        "last_name",
        "email",
        "groups",
        "is_superuser",
        "is_staff",
        "date_joined",
        "last_login"
    )

    readonly_fields = [
        "uuid",
        "email",
        "date_joined",
        "last_login"
    ]

    def get_readonly_fields(self, request, obj=None):
        ro_list = super(UtilisateurAdmin, self).get_readonly_fields(request, obj)
        #  On empêche les admins de devenir super-admin
        if not request.user.is_superuser:
            ro_list += ["is_superuser"]
        #  On empêche le super-admin de dégrader les autres super-admins
        if obj and request.user.is_superuser and obj.pk != request.user.pk:
            ro_list += ["is_superuser"]

        return ro_list

    def save_model(self, request, obj, form, change):
        try:
            obj_org = Utilisateur.objects.get(pk=obj.pk)
            if not request.user.is_superuser or (request.user.is_superuser and obj.pk != request.user.pk):
                obj.is_superuser = obj_org.is_superuser
        except Utilisateur.DoesNotExist:
            raise Utilisateur.DoesNotExist()
        super(UtilisateurAdmin, self).save_model(request, obj, form, change)
