from .system import custom_admin, CustomAdmin
from .citations import CitationAdmin
from .utilisateur import UtilisateurAdmin
