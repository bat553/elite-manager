from django.contrib import admin
from django.utils.timezone import now

from app_citations.admin import custom_admin
from app_citations.models import Citation, Log


@admin.register(Citation, site=custom_admin)
class CitationAdmin(admin.ModelAdmin):
    list_display = ["created_by", "__str__", "timestamp"]
    ordering = ["-timestamp", "created_by"]

    search_fields = ("created_by", "__str__",)

    list_filter = ["statut", "timestamp", ]

    fields = (
        "citation",
        "auteur",
        "date",
        "statut",
        "created_by",
        "timestamp",
        "edited"
    )

    readonly_fields = (
        "created_by",
        "timestamp",
        "edited"
    )

    def save_model(self, request, obj, form, change):
        if not obj.timestamp or not obj.created_by:
            edit = False
            obj.timestamp = now()
            obj.created_by = request.user
        else:
            edit = True
            obj.edited = now()

        super(CitationAdmin, self).save_model(request, obj, form, change)
        Log.objects.create(citation=obj, user=request.user, action="Edition" if edit else "Creation")
