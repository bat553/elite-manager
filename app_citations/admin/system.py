from django.contrib import admin

# Register your models here.
from django.contrib.admin.models import LogEntry
from django.contrib.auth.models import Group

from app_citations.models import Log


class CustomAdmin(admin.AdminSite):
    site_header = "Administration des citations élite"


custom_admin = CustomAdmin(name="custom_admin")


@admin.register(Group, site=custom_admin)
class GroupAdmin(admin.ModelAdmin):
    pass


@admin.register(Log, site=custom_admin)
class LogAdmin(admin.ModelAdmin):
    fields = (
        "user",
        "action",
        "timestamp",
        "citation",
        "data"
    )

    readonly_fields = (
        "user",
        "action",
        "timestamp",
        "citation",
        "data"
    )
    list_display = (
        "action",
        "user",
        "timestamp"
    )

    list_filter = ("action",)


@admin.register(LogEntry, site=custom_admin)
class LogEntryAdmin(admin.ModelAdmin):
    pass
