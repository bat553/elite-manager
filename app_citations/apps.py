from django.apps import AppConfig
from django.contrib.admin.apps import AdminConfig


class ApiConfig(AppConfig):
    name = "app_citations"


class CustomAdminConfig(AdminConfig):
    default_site = "app_citations.admin.CustomAdmin"
