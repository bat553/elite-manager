from django.urls import path

from app_citations import views

urlpatterns = [
    path("", views.ihm_index, name="index"),
    path("citation", views.citation_random, name="random_citation")
]
