from django.urls import path

from app_citations import views

urlpatterns = [path("citation", views.get_random, name="random")]
