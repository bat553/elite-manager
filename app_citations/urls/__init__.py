from django.urls import path, include

urlpatterns = [
    path("api/", include("app_citations.urls.urls_api")),
    path("oidc/", include("app_citations.urls.urls_oidc")),
    path("", include("app_citations.urls.urls_ihm")),
]
