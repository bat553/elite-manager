from django.urls import path

from app_citations.views import authentifications

urlpatterns = [
    path("discord/authorize", authentifications.login_discord, name="login_discord"),
    path("discord/callback", authentifications.callback_discord, name="callback_discord"),


    path("swano/authorize", authentifications.login_swano, name="login_swano"),
    path("swano/callback", authentifications.callback_swano, name="callback_swano"),
]
