"""elite_manager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.shortcuts import redirect
from django.urls import path, include

from app_citations.admin import custom_admin


def redirect_to_login(request):
    if request.user.is_anonymous:
        return redirect("/")
    return redirect("/admin/")


urlpatterns = [
    path("", include("app_citations.urls")),
    path("admin/login/", redirect_to_login),
    path("admin/", custom_admin.urls),
]
