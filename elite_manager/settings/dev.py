from .base import *


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": configuration["postgres"].get("name"),
        "USER": configuration["postgres"].get("user"),
        "PASSWORD": configuration["postgres"].get("password"),
        "HOST": configuration["postgres"].get("host"),
        "PORT": configuration["postgres"].get("port"),
    }
}
