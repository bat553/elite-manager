# Manager des citations élite
###### Une private joke qui va trop loin.

### Pré-requis :

* Base de données postgreSQL
* Identifiants oAuth2 discord

### Installation :
```
pip install -r requirements/dev.txt
cd elite_manager
mv configuration.empty.cfg configuration.cfg
```
Modifier `configuration.cfg` avec la bonne configuration
```
python manage.py makemigrations
python manage.py migrate
python manage.py init_app
python manage.py runserver
```

Se connecter à l'application avec son compte discord.
Modifier le compté créé suite à la connexion dans la base de donnée :
- `is_staff=True`
- `is_superuser=True`

Se connecter à l'administration sur `/admin`


- [To do List](TODO.md)
- [License](LICENSE.md)
- [Contributors](CONTRIBUTORS.md) 
